# Generated by Django 3.1.4 on 2021-01-09 16:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('table_3', '0006_auto_20210109_1855'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='spr_incident_type',
            new_name='SprIncidentType',
        ),
    ]
