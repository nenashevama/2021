from django.contrib import admin
from table_3.models import SprIncidentType, Table3

admin.site.register(SprIncidentType)
admin.site.register(Table3)