from django.db import models
from django.contrib.auth.models import User


class Units(models.Model):
    id = models.IntegerField('Идентификатор ', primary_key=True)
    user = models.ForeignKey(User, null=True, blank=True,
                                on_delete=models.SET_NULL)
    name =  models.CharField('Наименование', max_length=50)
    cam_count = models.IntegerField('Количество камер')
    cam_work = models.IntegerField('Количество работающих камер')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Подразделение'
        verbose_name_plural = 'Подразделения'


class UsersProfiles(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    unit = models.ForeignKey(Units, null=True, blank=True,  on_delete=models.SET_NULL)
    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'
