from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return render(request, 'report/index.html',locals())

def about(request):
    return render(request, 'report/about.html')

def login(request):
    return render(request, 'registration/login.html',locals())

def logout(request):
    return render(request, 'registration/logged_out.html',locals())

def create(request):
    return render(request, 'table/create.html',locals())


