from django.contrib import admin
from report.models import Units, UsersProfiles

admin.site.register(Units)
admin.site.register(UsersProfiles)